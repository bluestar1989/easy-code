import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import {appRouter, adminRouter, otherRouter} from '@/router/routes'

export default {
  state: {
    routers: [
      otherRouter,
      ...appRouter,
      ...adminRouter
    ],
    menuList: [],
    tagsList: [...otherRouter.children],
    pageOpenedList: [{
      title: '首页',
      path: '',
      name: 'home_index'
    }],
    currentPageName: '',
    // 面包屑数组
    currentPath: [
      {
        title: '首页',
        path: '',
        name: 'home_index'
      }
    ],
    // 要展开的菜单数组
    openedSubmenuArr: [],
    // 主题
    menuTheme: '',
    theme: '',
    cachePage: [],
    lang: '',
    isFullScreen: false,
    // 在这里定义你不想要缓存的页面的name属性值(参见路由配置router.js)
    dontCache: ['text-editor']
  },
  getters,
  actions,
  mutations
}
