import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import dictionary from './modules/dictionary'
import iviewAdmin from './modules/iviewAdmin'
import login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    language: 'zh-CN'
  },
  getters,
  actions,
  mutations,
  modules: {
    dictionaryStore: dictionary,
    iviewAdminStore: iviewAdmin,
    loginStore: login
  }
})
