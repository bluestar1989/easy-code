package com.jeasy;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import com.jeasy.common.Func;
import com.jeasy.common.charset.CharsetKit;
import com.jeasy.common.pinyin.PinYinKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.common.template.TemplateKit;

import java.util.List;
import java.util.Map;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/4/8 下午3:00
 */
public class DbInitMain {
    public static void main(String[] args) {
//        initDictionary();
        initResourcesAndRole();
//        genDictionaryKit();
    }

    private static final String DICTIONARY_PARAMS =
        "用户状态:[1000=启用,1001=停用]|" +
            "机构类型:[2000=其他]";

    private static void initDictionary() {
        String[] dictionaryParams = StrKit.split(DICTIONARY_PARAMS, "|");

        // 主键ID从1开始递增
        int id = 1;
        for (String dictionary : dictionaryParams) {
            String[] dictionaryParts = StrKit.split(dictionary, ":");
            String subDictionary = StrKit.S_EMPTY;
            String parentDictionary = StrKit.S_EMPTY;
            // 判断是否符合XXXX:0000=YYYY[1111=AAAA,2222=BBBB]
            if (StrKit.containsIgnoreCase(dictionaryParts[1], "[")) {
                if (StrKit.startWith(dictionaryParts[1], "[", false)) {
                    // 处理XXXX:[1111=AAAA,2222=BBBB]，无父字典情况
                    subDictionary = StrKit.sub(dictionaryParts[1], 1, dictionaryParts[1].length() - 1);
                } else {
                    // 处理XXXX:0000=YYYY[1111=AAAA,2222=BBBB]，有父字典情况
                    parentDictionary = StrKit.subPre(dictionaryParts[1], dictionaryParts[1].indexOf("["));
                    subDictionary = StrKit.sub(dictionaryParts[1], dictionaryParts[1].indexOf("[") + 1, dictionaryParts[1].length() - 1);
                }
            }

            int pid = 0;
            if (Func.isNotEmpty(parentDictionary)) {
                // 优先插入父字典
                String[] parts = StrKit.split(parentDictionary, "=");

                pid = id++;
                System.out.println(
                    StrKit.format("INSERT INTO `bd_dictionary` " +
                            "(`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                            "VALUES " +
                            "({}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                        pid, parts[1], PinYinKit.getPinYinHeadChar(parts[1]).toUpperCase(), Integer.valueOf(parts[0].trim()), PinYinKit.getPinYinHeadChar(dictionaryParts[0]).toUpperCase(), dictionaryParts[0], 0, 0, "", System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

            }

            if (Func.isNotEmpty(subDictionary)) {
                // 插入子字典
                String[] subDictionaryParams = StrKit.split(subDictionary, ",");
                for (String subDictionaryParam : subDictionaryParams) {
                    String[] parts = StrKit.split(subDictionaryParam, "=");
                    System.out.println(
                        StrKit.format("INSERT INTO `bd_dictionary` " +
                                "(`id`, `name`, `code`, `value`, `type`, `typeName`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                "VALUES " +
                                "({}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                            id++, parts[1], PinYinKit.getPinYinHeadChar(parts[1]).toUpperCase(), Integer.valueOf(parts[0].trim()), PinYinKit.getPinYinHeadChar(dictionaryParts[0]).toUpperCase(), dictionaryParts[0], 0, pid, "", System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                }
            }
        }
    }

    private static void genDictionaryKit() {
        Map<String, List<DicType>> model = Maps.newHashMap();
        List<DicType> dicTypes = Lists.newArrayList();
        model.put("dicTypes", dicTypes);
        String[] dictionaryParams = StrKit.split(DICTIONARY_PARAMS, "|");
        for (String dictionaryParam : dictionaryParams) {
            String[] dictionaryParts = StrKit.split(dictionaryParam, ":");
            StringBuilder dictionaryInfo = new StringBuilder();
            // 判断是否符合XXXX:0000=YYYY[1111=AAAA,2222=BBBB]
            if (StrKit.containsIgnoreCase(dictionaryParts[1], "[")) {
                if (StrKit.startWith(dictionaryParts[1], "[", false)) {
                    // 处理XXXX:[1111=AAAA,2222=BBBB]，无父字典情况
                    dictionaryInfo.append(StrKit.sub(dictionaryParts[1], 1, dictionaryParts[1].length() - 1));
                } else {
                    // 处理XXXX:0000=YYYY[1111=AAAA,2222=BBBB]，有父字典情况
                    dictionaryInfo.append(StrKit.subPre(dictionaryParts[1], dictionaryParts[1].indexOf("[")));
                    dictionaryInfo.append(",").append(StrKit.sub(dictionaryParts[1], dictionaryParts[1].indexOf("[") + 1, dictionaryParts[1].length() - 1));
                }
            }
            String[] dictionarys = StrKit.split(dictionaryInfo.toString(), ",");

            DicType dicType = new DicType();
            dicType.setName(dictionaryParts[0]);
            dicType.setCode(PinYinKit.getPinYinHeadChar(dictionaryParts[0]).toUpperCase());
            dicTypes.add(dicType);

            List<Dic> dics = Lists.newArrayList();
            for (String dictionary : dictionarys) {
                String[] dictionaryItems = StrKit.split(dictionary, "=");
                Dic dic = new Dic();
                Map<String, String> params = Maps.newHashMap();
                params.put("\\(", "_");
                params.put("\\)", "");

                dic.setName(dictionaryItems[1]);
                dic.setCode(StrKit.replace(PinYinKit.getPinYinHeadChar(dictionaryItems[1]).toUpperCase(), params));
                dic.setOrigCode(PinYinKit.getPinYinHeadChar(dictionaryItems[1]).toUpperCase());
                dics.add(dic);
            }
            dicType.setDics(dics);
        }

        TemplateKit.executeFreemarker("/Users/TaoBangren/git@osc/easy-code/jeasy-code-gen/src/main/resources", "DictionaryKit.ftl", CharsetKit.DEFAULT_ENCODE, model, "/Users/TaoBangren/git@osc/easy-code/jeasy-code-gen/src/main/java/com/jeasy", "DictionaryKit.java");
    }

    private static final String RESOURCE_PARAMS =
        "用户管理(ios-people)=" +
            "{人员管理(ios-body-outline&/user)[查询-查看-添加-修改-删除]};" +
            "{角色管理(ios-person-outline&/role)[查询-查看-添加-修改-删除]};" +
            "{组织机构(ios-people-outline&/organization)[查询-查看-添加-修改-删除]};" +
            "{菜单权限(android-menu&/resource)[查询-查看-添加-修改-删除]}|" +
            "基础数据(settings)=" +
            "{公共码表(ios-book-outline&/dictionary)[查询-查看-添加-修改-删除]}|" +
            "代码平台(code-working)=" +
            "{接口文档(usb&/api)[查询]};" +
            "{代码生成(code-download&/code)[查询]}|" +
            "日志监控(monitor)=" +
            "{操作日志(ios-paper&/log)[查询]};" +
            "{数据监控(ios-analytics&/druid)[查询]};" +
            "{接口监控(usb&/monitor)[查询]}";

    private static void initResourcesAndRole() {
        String[] menus = StrKit.split(RESOURCE_PARAMS, "|");

        int menuId = 1;
        int roleResourceId = 1;
        for (String menu : menus) {
            String[] menuParts = menu.split("=");
            String menuName = menuParts[0];
            String icon = menuName.substring(menuName.lastIndexOf("(") + 1, menuName.lastIndexOf(")"));
            menuName = menuName.substring(0, menuName.lastIndexOf("("));

            // 处理一级菜单
            System.out.println(
                StrKit.format("INSERT INTO `su_resource` " +
                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                        "VALUES " +
                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                    menuId, menuName, PinYinKit.getPinYinHeadChar(menuName).toUpperCase(), "", icon, "", 0, 0, 1, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                    "VALUES " +
                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                roleResourceId++, 1, "超级管理员", "CJGLY", menuId, menuName, PinYinKit.getPinYinHeadChar(menuName).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

            int pid = menuId++;
            String[] subMenus = menuParts[1].split(";");
            for (String subMenuInfo : subMenus) {

                String[] subMenuParts = subMenuInfo.split("\\[");
                String subMenuName = subMenuParts[0].substring(1);
                icon = subMenuName.substring(subMenuName.lastIndexOf("(") + 1, subMenuName.lastIndexOf("&"));
                String url = subMenuName.substring(subMenuName.lastIndexOf("&") + 1, subMenuName.lastIndexOf(")"));
                subMenuName = subMenuName.substring(0, subMenuName.lastIndexOf("("));

                // 处理二级菜单
                System.out.println(
                    StrKit.format("INSERT INTO `su_resource` " +
                            "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                            "VALUES " +
                            "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                        menuId, subMenuName, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase(), url, icon, "", pid, 0, 1, 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                        "VALUES " +
                        "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                    roleResourceId++, 1, "超级管理员", "CJGLY", menuId, subMenuName, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                int parentMenuId = menuId++;
                String[] tabInfos = subMenuParts[1].substring(0, subMenuParts[1].lastIndexOf("]")).split(",");
                for (String tabInfo : tabInfos) {

                    String[] options;
                    if (tabInfo.contains(":")) {
                        String[] tabParts = tabInfo.split(":");
                        String tabName = tabParts[0];
                        // 处理二级菜单下的选项卡
                        System.out.println(
                            StrKit.format("INSERT INTO `su_resource` " +
                                    "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                    "VALUES " +
                                    "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                menuId, tabName, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(tabName).toUpperCase(), "", "", "", parentMenuId, 0, 0, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                        System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                "VALUES " +
                                "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                            roleResourceId++, 1, "超级管理员", "CJGLY", menuId, tabName, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(tabName).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                        int optionParentMenuId = menuId++;
                        // 处理选项卡下的操作按钮
                        options = StrKit.split(tabParts[1], "-");
                        for (String option : options) {
                            System.out.println(
                                StrKit.format("INSERT INTO `su_resource` " +
                                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                        "VALUES " +
                                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                    menuId, option, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(tabName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(option).toUpperCase(), "", "", "", optionParentMenuId, 0, 0, 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                    "VALUES " +
                                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                                roleResourceId++, 1, "超级管理员", "CJGLY", menuId++, option, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(tabName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(option).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                        }
                    } else {
                        // 处理二级菜单下的操作按钮
                        options = StrKit.split(tabInfo, "-");

                        for (String option : options) {
                            System.out.println(
                                StrKit.format("INSERT INTO `su_resource` " +
                                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                        "VALUES " +
                                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                    menuId, option, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(option).toUpperCase(), "", "", "", parentMenuId, 0, 0, 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) " +
                                    "VALUES " +
                                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                                roleResourceId++, 1, "超级管理员", "CJGLY", menuId++, option, PinYinKit.getPinYinHeadChar(menuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenuName).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(option).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                        }
                    }
                }
            }
        }
    }
}
