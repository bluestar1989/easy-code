package com.jeasy.dictionary;

import com.google.common.collect.Maps;
import com.jeasy.dictionary.dto.DictionaryDTO;
import com.jeasy.dictionary.manager.DictionaryManager;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * 字典工具类
 *
 * @author TaoBangren
 * @version 1.0
 * @since 2017/4/26 上午10:20
 */
@Slf4j
public final class DictionaryKit {

    /**
     * YES
     */
    public static final Integer YES = 1;

    /**
     * NO
     */
    public static final Integer NO = 0;

    /**
     * 用户状态.
     */
    public static final String TYPE_YHZT = "YHZT";

    /**
     * 用户状态-启用.
     */
    public static final String YHZT_QY = "QY";

    /**
     * 用户状态-停用.
     */
    public static final String YHZT_TY = "TY";

    /**
     * 用户状态字典集合.
     */
    public static List<DictionaryDTO> YHZT_DICS() {
        return DictionaryManager.me().findByType(TYPE_YHZT);
    }

    /**
     * 用户状态字典MAP.
     */
    public static Map<String, DictionaryDTO> YHZT_MAP() {
        List<DictionaryDTO> dictionaryDTOList = YHZT_DICS();
        Map<String, DictionaryDTO> YHZT_MAP = Maps.newHashMap();
        for (DictionaryDTO dictionaryDTO : dictionaryDTOList) {
            YHZT_MAP.put(dictionaryDTO.getCode(), dictionaryDTO);
        }
        return YHZT_MAP;
    }

    /**
     * 用户状态-启用.
     */
    public static DictionaryDTO YHZT_QY() {
        return DictionaryManager.me().getByCode(TYPE_YHZT, YHZT_QY);
    }

    /**
     * 用户状态-启用ID.
     */
    public static Long YHZT_QY_ID() {
        return YHZT_QY().getId();
    }

    /**
     * 用户状态-启用名称.
     */
    public static String YHZT_QY_NAME() {
        return YHZT_QY().getName();
    }

    /**
     * 用户状态-启用值.
     */
    public static Integer YHZT_QY_VAL() {
        return YHZT_QY().getValue();
    }

    /**
     * 用户状态-停用.
     */
    public static DictionaryDTO YHZT_TY() {
        return DictionaryManager.me().getByCode(TYPE_YHZT, YHZT_TY);
    }

    /**
     * 用户状态-停用ID.
     */
    public static Long YHZT_TY_ID() {
        return YHZT_TY().getId();
    }

    /**
     * 用户状态-停用名称.
     */
    public static String YHZT_TY_NAME() {
        return YHZT_TY().getName();
    }

    /**
     * 用户状态-停用值.
     */
    public static Integer YHZT_TY_VAL() {
        return YHZT_TY().getValue();
    }

    /**
     * 机构类型.
     */
    public static final String TYPE_JGLX = "JGLX";

    /**
     * 机构类型-其他.
     */
    public static final String JGLX_QT = "QT";

    /**
     * 机构类型字典集合.
     */
    public static List<DictionaryDTO> JGLX_DICS() {
        return DictionaryManager.me().findByType(TYPE_JGLX);
    }

    /**
     * 机构类型字典MAP.
     */
    public static Map<String, DictionaryDTO> JGLX_MAP() {
        List<DictionaryDTO> dictionaryDTOList = JGLX_DICS();
        Map<String, DictionaryDTO> JGLX_MAP = Maps.newHashMap();
        for (DictionaryDTO dictionaryDTO : dictionaryDTOList) {
            JGLX_MAP.put(dictionaryDTO.getCode(), dictionaryDTO);
        }
        return JGLX_MAP;
    }

    /**
     * 机构类型-其他.
     */
    public static DictionaryDTO JGLX_QT() {
        return DictionaryManager.me().getByCode(TYPE_JGLX, JGLX_QT);
    }

    /**
     * 机构类型-其他ID.
     */
    public static Long JGLX_QT_ID() {
        return JGLX_QT().getId();
    }

    /**
     * 机构类型-其他名称.
     */
    public static String JGLX_QT_NAME() {
        return JGLX_QT().getName();
    }

    /**
     * 机构类型-其他值.
     */
    public static Integer JGLX_QT_VAL() {
        return JGLX_QT().getValue();
    }
}
